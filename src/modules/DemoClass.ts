/////////////////////////
// DemoClass - demonstrates "not a constructor if getProvider() is called"

import {Provider,getProvider} from '@decentraland/web3-provider'

export {DemoClass}

class DemoClass{
    provider:Provider
    constructor() {
    }

    method1() {
        // Enabling the following line of code causes the instantiation of this class to report, at runtime,
        // "TypeError: DemoClass_1.DemoClass is not a constructor".
        // This is true even though this method is never called and isn't in the constructor.
        this.provider = getProvider()
    }
}