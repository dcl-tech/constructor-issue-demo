/////////////////////////////////////////////////////
// Demo of "not a contructor" problem
// There is a line of code in a method in the DemoClass mpodule
// which causes the runtime to report that DemoClass isn't a constructor.
/////////////////////////////////////////////////////

import {DemoClass} from './modules/DemoClass'

// // If the class is defined in game.ts, the issue does not occur.
// // It only shows up if the class lives in a module.
// import {Provider,getProvider} from '@decentraland/web3-provider'
// class DemoClass{
//     provider:Provider
//     constructor() {
//     }

//     method1() {
//         // Enabling the following line of code causes the instantiation of this class to report, at runtime,
//         // "TypeError: DemoClass_1.DemoClass is not a constructor", but only if the class lives in a module.
//         // This is true even though this method is never called and isn't in the constructor.
//         this.provider = getProvider()
//     }
// }

// if 0.2 SUCCESS doesn't print and the box doesn't appear then you are seeing the issue

log("-------0.1---------")
let demoClass:DemoClass = new DemoClass()
log("-------0.2 SUCCESS ---------")

let box = new Entity()
box.addComponent(new BoxShape)
box.addComponent(new Transform({position:new Vector3(8,1,8)}))
engine.addEntity(box)

