This is a demo of an issue in which a class (DemoClass) won't instantiate from a module if getProvider() is called from a method in that class.
It occurs both in preview and when deployed, so it isn't unique to the Preview.
In fact, getProvider is never actualluy called, because the method of the class is never called.
It may have something to do with module loading order.

This problem does NOT occur if instead the DemoClass lives with games.ts instead of residing in a module.

